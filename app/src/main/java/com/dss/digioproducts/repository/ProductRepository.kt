package com.dss.digioproducts.repository

import com.dss.digioproducts.model.ProductList
import java.lang.Exception

class ProductRepository(private val productsAPI: ProductsAPI) {

    suspend fun getProducts() : ProductList?{
        return try{
            productsAPI.getProductsData()
        }catch (e : Exception){
            null
        }
    }
}