package com.dss.digioproducts.repository

import com.dss.digioproducts.model.ProductList
import retrofit2.Call
import retrofit2.http.GET

interface ProductsAPI {

    @GET("products")
    suspend fun getProductsData() : ProductList
}