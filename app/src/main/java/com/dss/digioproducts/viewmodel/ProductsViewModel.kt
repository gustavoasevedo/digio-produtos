package com.dss.digioproducts.viewmodel

import androidx.lifecycle.*
import com.dss.digioproducts.model.ProductList
import com.dss.digioproducts.repository.ProductRepository
import kotlinx.coroutines.launch

class ProductsViewModel(private val repository: ProductRepository) : ViewModel() {

    var viewState = MutableLiveData<ProductViewState>()

    var products: MutableLiveData<ProductList> =  MutableLiveData()

    fun getProducts(){
        viewModelScope.launch {
            viewState.postValue(ProductViewState.runProgress)
            products.postValue(repository.getProducts())
            viewState.postValue(ProductViewState.stopProgress)
        }
    }
}