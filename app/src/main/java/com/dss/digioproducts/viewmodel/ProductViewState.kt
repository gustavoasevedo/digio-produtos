package com.dss.digioproducts.viewmodel

sealed class ProductViewState {

    object runProgress : ProductViewState()
    object stopProgress : ProductViewState()

}