package com.dss.digioproducts.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dss.digioproducts.R
import com.dss.digioproducts.view.fragment.ProductsFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer,ProductsFragment::class.java,null,null)
            .commit()
    }
}