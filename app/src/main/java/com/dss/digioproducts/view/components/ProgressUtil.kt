package com.dss.digioproducts.view.components

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import androidx.core.view.get
import com.dss.digioproducts.R

/**
 * Created by Gustavo Asevedo on 28/08/2020.
 */
class ProgressUtil(var activity: Activity) {

    private var progress: View? = null

    fun runProgress() {
        if (progress == null) {
            val view: View = activity.findViewById<View>(android.R.id.content).rootView
            createGlobalProgress(view)
        }
    }

    fun stopProgressOnView() {
        if (progress != null) {
            val rootview = progress!!.rootView as ViewGroup
            hideGlobalProgress(rootview)
        }
    }

    private fun createGlobalProgress(view: View) {
        val inflater = LayoutInflater.from(activity)
        val progress: View = inflater.inflate(R.layout.dialog_progress, view as ViewGroup, true)
        this.progress = progress

        activity.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    private fun hideGlobalProgress(rootView: ViewGroup) {
        if (progress != null) {

            removeGlobalProgress(rootView)

            progress = null

            activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }


    private fun removeGlobalProgress(rootview: ViewGroup) {
        for (count in 0 until rootview.childCount) {
            var progressRemoved = false
            if (rootview[count] is ViewGroup) {
                val childView = rootview[count] as ViewGroup
                for (countChild in 0 until childView.childCount) {
                    val view = childView[countChild]
                    if (view is ProgressBar) {
                        rootview.removeViewAt(count)
                        progressRemoved = true
                        break
                    }
                }
            }
            if (progressRemoved) break
        }
    }
}