package com.dss.digioproducts.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.dss.digioproducts.R
import com.dss.digioproducts.model.Product
import kotlinx.android.synthetic.main.item_pager_spotlight.*

/**
 * Created by Gustavo Asevedo on 29/08/2020.
 */

class PagerItemFragment(val product: Product, val config : Int) : Fragment(){

    companion object{
        const val SPOTLIGHT = 1
        const val PRODUCT = 2
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return if(config == SPOTLIGHT){
            layoutInflater.inflate(R.layout.item_pager_spotlight, container, false)
        }else{
            layoutInflater.inflate(R.layout.item_pager_product, container, false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this)
            .load(product.bannerURL)
            .transform(RoundedCorners(30))
            .error(ContextCompat.getDrawable(activity!!,R.drawable.image_error))
            .placeholder(ContextCompat.getDrawable(activity!!,R.drawable.image_placeholder))
            .into(imgPager)

        imgPager.contentDescription = product.description
    }

}