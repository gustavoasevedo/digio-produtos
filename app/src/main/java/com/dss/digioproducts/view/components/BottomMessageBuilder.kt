package com.dss.digioproducts.view.components

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.dss.digioproducts.R
import com.google.android.material.bottomsheet.BottomSheetDialog

/**
 * Created by Gustavo Asevedo on 29/08/2020.
 */
class BottomMessageBuilder {

    private lateinit var dialog: BottomSheetDialog

    fun with(context: Context): BottomMessageBuilder {
        dialog = BottomSheetDialog(context)
        dialog.setContentView(R.layout.dialog_error)
        return this
    }

    fun Title(title: String): BottomMessageBuilder {
        val txtTitle: TextView = dialog.findViewById(R.id.txtTitle)!!
        txtTitle.text = title
        return this
    }

    fun Content(text: String): BottomMessageBuilder {
        val txtContent: TextView = dialog.findViewById(R.id.txtContent)!!
        txtContent.text = text
        return this
    }

    fun Action(buttonText: String, action: View.OnClickListener): BottomMessageBuilder {
        var btnAction: Button = dialog.findViewById(R.id.btnAction)!!
        btnAction.text = buttonText
        btnAction.setOnClickListener {
            action.onClick(it)
            dialog.dismiss()
        }
        btnAction.visibility = View.VISIBLE
        return this
    }

    fun build(): BottomMessageBuilder {
        dialog.show()
        return this
    }

}