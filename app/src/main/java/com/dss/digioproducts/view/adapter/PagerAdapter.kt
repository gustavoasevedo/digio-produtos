package com.dss.digioproducts.view.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dss.digioproducts.model.Product
import com.dss.digioproducts.view.fragment.PagerItemFragment

/**
 * Created by Gustavo Asevedo on 29/08/2020.
 */
class PagerAdapter(val list: ArrayList<Product>, val fm: FragmentManager, val config: Int) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Fragment {
        return PagerItemFragment(list[position], config)
    }

}