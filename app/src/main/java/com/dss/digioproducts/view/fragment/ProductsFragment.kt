package com.dss.digioproducts.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.dss.digioproducts.R
import com.dss.digioproducts.model.Product
import com.dss.digioproducts.model.ProductList
import com.dss.digioproducts.view.adapter.PagerAdapter
import com.dss.digioproducts.view.components.BottomMessageBuilder
import com.dss.digioproducts.view.components.ProgressUtil
import com.dss.digioproducts.viewmodel.ProductViewState
import com.dss.digioproducts.viewmodel.ProductsViewModel
import kotlinx.android.synthetic.main.fragment_products.*
import org.koin.android.viewmodel.ext.android.viewModel


class ProductsFragment : Fragment() {

    private val productsViewModel by viewModel<ProductsViewModel>()
    private lateinit var progress : ProgressUtil

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layoutInflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progress = ProgressUtil(activity!!)

        productsViewModel.products.observe(viewLifecycleOwner, productsObserver)
        productsViewModel.viewState.observe(viewLifecycleOwner, stateObserver)

        productsViewModel.getProducts()
    }

    private var productsObserver = Observer<ProductList> {
        if (it != null) {
            configureCash(it.cash)
            configureSpotlight(it.spotlight)
            configureProducts(it.products)
        } else {
            BottomMessageBuilder().with(context!!)
                .Title(getString(R.string.error_title))
                .Content(getString(R.string.error_message))
                .Action(getString(R.string.error_action)) { productsViewModel.getProducts() }
                .build()
        }
    }

    private fun configureSpotlight(spotlight: ArrayList<Product>) {
        val pagerAdapter = PagerAdapter(spotlight, activity!!.supportFragmentManager, PagerItemFragment.SPOTLIGHT)
        pagerSpotlight.adapter = pagerAdapter
        pagerSpotlight.offscreenPageLimit = 3
        pagerProducts.clipToPadding = false
    }

    private fun configureProducts(products: ArrayList<Product>) {
        val pagerAdapter = PagerAdapter(products, activity!!.supportFragmentManager, PagerItemFragment.PRODUCT)
        pagerProducts.adapter = pagerAdapter
        pagerProducts.offscreenPageLimit = 3
        pagerProducts.clipToPadding = false
        pagerProducts.setPadding(0, 80, 650, 80)
        pagerProducts.pageMargin = 36

    }

    private fun configureCash(product: Product){

        txtCashTitle.text = HtmlCompat.fromHtml(
            getString(R.string.cash_title),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )

        Glide.with(this)
            .load(product.bannerURL)
            .centerCrop()
            .error(ContextCompat.getDrawable(activity!!,R.drawable.image_error))
            .placeholder(ContextCompat.getDrawable(activity!!,R.drawable.image_placeholder))
            .transform(RoundedCorners(30))
            .into(imgCash)

        imgCash.contentDescription = product.description
    }

    private var stateObserver = Observer<ProductViewState> {
        when (it) {
            is ProductViewState.runProgress -> {
                progress.runProgress()
            }
            is ProductViewState.stopProgress -> {
                progress.stopProgressOnView()
            }
        }
    }

}