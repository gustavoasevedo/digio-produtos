package com.dss.digioproducts.model


import com.google.gson.annotations.SerializedName


data class Product(
    @SerializedName(value = "bannerURL", alternate = ["imageURL"]) val bannerURL: String,
    @SerializedName("description") val description: String,
    @SerializedName(value = "title", alternate = ["name"]) val title: String
)