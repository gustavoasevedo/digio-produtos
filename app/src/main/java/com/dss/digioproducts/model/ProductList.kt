package com.dss.digioproducts.model


import com.google.gson.annotations.SerializedName

data class ProductList(
    @SerializedName("cash") val cash: Product,
    @SerializedName("products") val products: ArrayList<Product>,
    @SerializedName("spotlight") val spotlight: ArrayList<Product>
)