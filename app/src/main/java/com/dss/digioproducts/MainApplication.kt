package com.dss.digioproducts

import android.app.Application
import com.dss.digioproducts.modules.*
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.internal.common.CrashlyticsCore
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Gustavo Asevedo on 27/08/2020.
 */
class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(this)
        FirebaseAnalytics.getInstance(this)

        startKoin {
            androidContext(this@MainApplication)
            modules(listOf(
                networkModule,
                productModule
            ))
        }
    }
}