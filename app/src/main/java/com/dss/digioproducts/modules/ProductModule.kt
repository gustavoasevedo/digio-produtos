package com.dss.digioproducts.modules

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.dss.digioproducts.repository.ProductRepository
import com.dss.digioproducts.view.components.ProgressUtil
import com.dss.digioproducts.view.fragment.ProductsFragment
import com.dss.digioproducts.viewmodel.ProductsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Gustavo Asevedo on 28/08/2020.
 */
val productModule = module {
    viewModel { ProductsViewModel(get()) }
    factory { ProductRepository(get()) }
    factory { ProductsFragment() }
}